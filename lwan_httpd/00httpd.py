#!/usr/bin/env python3
import lwan

class MyHandler(lwan.Handler):
  def handle_request(self, request):
    response = lwan.Response()
    response.set_body("Hello, world!")
    return response

def main(args):
  """
  Default port == 8000
  """
  server = lwan.Server()
  server.add_handler("/", MyHandler())
  server.start()

if __name__ == "__main__":
  main(argv[1:])

