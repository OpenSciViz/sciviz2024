#!/bin/bash
echo non-podman docker build best run as root
docker ps -a
docker images -a
echo 'docker build -t hon/tstrac:v0.0 ./'
docker build -t hon/tstrac:v0.0 ./
docker ps -a
docker images -a
echo 'docker run -d -p8080:8080 -p 8123:8123 --name hontstrac hon/tstrac:v0.0'
docker run -dit -p8080:8080 -p 8123:8123 --name hontstrac -v /var/www/html:/home/www hon/tstrac:v0.0
docker ps -a
echo 'docker exec -it hontstrac /bin/bash'
echo 'Should the bash exec fail, check that ports 8080 and/or 8123 are in use by whatever and killall...'

echo 'https://grafana.com/grafana/dashboards/'
echo 'docker pull grafana/grafana-oss-dev:11.0.0-167059-ubuntu'
echo 'change the default port from 3000 to a custom one by editing the grafana.ini'

