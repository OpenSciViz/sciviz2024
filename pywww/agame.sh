#!/bin/bash
echo https://pygame-web.github.io/wiki/pygbag/
echo 'pip3 install pygbag --user --upgrade'
echo 'mkdir agame && pushd agame'
echo 'Run command "pygbag folder_name" -- replace folder_name with the name that you gave your game folder'
echo 'If pygbag isn’t recognised as a command, you can run python -m pygbag folder_name as an alternative.'
echo 'After running this command, navigate to localhost:8000#debug'

