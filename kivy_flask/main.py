#!/usr/bin/env python3
import os, requests
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.filechooser import FileChooserIconView

class RootLayout(BoxLayout):
  def __init__(self, **kwargs):
    super(RootLayout, self).__init__(**kwargs)

    self.orientation = 'vertical'
    self.file_chooser = FileChooserIconView()
    self.file_chooser.bind(selection=self.on_file_selection)
    self.add_widget(self.file_chooser)
    self.image_display = Image()
    self.add_widget(self.image_display)
    self.upload_button = Button(text='Upload Image')
    self.upload_button.bind(on_release=self.upload_image)
    self.add_widget(self.upload_button)

  def on_file_selection(self, instance, value):
    if value: self.image_display.source = value[0]

  def upload_image(self, *args):
    if self.image_display.source: filename = os.path.basename(self.image_display.source)
    with open(self.image_display.source, 'rb') as f:
      files = {'image': (filename, f)}
      requests.post('Your Endpoint', files=files)
    return files

class MyApp(App):
  def build(self):
    layout = RootLayout()
    return layout

if __name__ == '__main__':
    MyApp().run()


