import os
from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.image import Image
from kivy.uix.filechooser import FileChooserIconView

class RootLayout(BoxLayout):
    def __init__(self, **kwargs):
        super(RootLayout, self).__init__(**kwargs)

        self.orientation = 'vertical'

        self.file_chooser = FileChooserIconView()
        self.file_chooser.bind(selection=self.on_file_selection)

        self.add_widget(self.file_chooser)

        self.image_display = Image()
        self.add_widget(self.image_display)

        self.upload_button = Button(text='Upload Image')
        self.upload_button.bind(on_release=self.upload_image)
        self.add_widget(self.upload_button)

    def on_file_selection(self, instance, value):
        if value:
            self.image_display.source = value[0]

    def upload_image(self, *args):
        if self.image_display.source:
            filename = os.path.basename(self.image_display.source)
            os.system(f'curl -F "image=@{self.image_display.source}" http://localhost:5000/upload')

class MyApp(App):
    def build(self):
        return RootLayout()

if __name__ == '__main__':
    MyApp().run()
