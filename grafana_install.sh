#!/bin/bash 
echo 'https://grafana.com/docs/grafana/latest/setup-grafana/installation/debian/'
echo 'apt install -y apt-transport-https software-properties-common wget'
echo 'mkdir -p /etc/apt/keyrings/'
echo 'wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null'
echo 'echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list'
# Updates the list of available packages
echo 'apt update'
# Installs the latest OSS release:
echo 'apt install grafana'
#
echo 'https://grafana.com/docs/grafana/latest/setup-grafana/start-restart-grafana/'
echo 'systemctl daemon-reload'
echo 'systemctl start grafana-server'
echo 'systemctl status grafana-server'
echo 'systemctl enable grafana-server.service'
echo 'systemctl restart grafana-server'
echo './bin/grafana server'
echo 'docker restart grafana'

