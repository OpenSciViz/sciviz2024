#!/usr/bin/env python3
# from https://softwareengineering.stackexchange.com/questions/418600/best-practice-for-python-main-function-definition-and-program-start-exit
import os, sys, yaml  # just used as an example here for loading more configs, optional
from argparse import ArgumentParser, Namespace
from typing import Dict, List

def parse_args(cli_args: List[str] = None) -> Namespace:
  parser = ArgumentParser()
  # parser.add_argument()
  # ...
  return parser.parse_args(args=cli_args)  # None defaults to sys.argv[1:]

def load_configs(args: Namespace) -> Dict:
  sane_defaults = []
  try:
    with open(args.config_path, 'r') as file_pointer:
      config = yaml.safe_load(file_pointer)
    # arrange and check configs here
    return config
  except Exception as err:
    print(err) # log errors
    raise err
  # return some sane fallback defaults if desired/reasonable
  return sane_defaults
# end load_configs

def main(args: Namespace = parse_arguments()) -> int:
  try:
    # load some additional config files here or in a function called here
    # e.g. args contains a path to a config folder; or use sane defaults
    # if the config files are missing(if that is your desired behavior)
    config = load_configs(args)
    do_real_work(args, config)

  except KeyboardInterrupt:
    print("aborted manually.", file=sys.stderr)
    return 1

  except Exception as err:
    # (in real code the `except` would probably be less broad)
    # Turn exceptions into appropriate logs and/or console output.
    # log err
    print("unhandled exception:", err)
    # non-zero return code to signal error
    # Can of course be more fine grained than this general
    # "something went wrong" code.
    return -1

  return True # success
# end main

if __name__ == "__main__":
  sys.exit(main(parse_args()))
