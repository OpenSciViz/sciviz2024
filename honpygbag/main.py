#!/usr/bin/env python3
import asyncio

def init():
  try:
    import pygame
    pygame.init()
    pygame.display.set_mode((320, 240))
    clock = pygame.time.Clock()
  except: return None

  return clock

async def main(count=60):
  clock = init()
  if not(clock): return -1
  while True:
    print(f"{count}: Hello from Pygame")
    pygame.display.update()
    await asyncio.sleep(0)  # You must include this statement in your main loop. Keep the argument at 0.
    if not count:
      pygame.quit()
      return
        
    count -= 1 ; clock.tick(60)
  # endwhile
  return count

if __name__ == "__main__":
  """
  from https://pygame-web.github.io/wiki/pygbag/
  Run this command: pygbag folder_name (replace folder_name with the name that you gave your game folder)
  If pygbag isn’t recognised as a command, you can run python -m pygbag folder_name as an alternative.
  After running this command, navigate to localhost:8000#debug.
  """
  # asyncio.run(main(parse_args()))
  asyncio.run(main())

