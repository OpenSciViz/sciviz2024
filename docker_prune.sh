#!/bin/bash
# https://depot.dev/blog/docker-clear-cache
docker images
docker ps -a
docker ps --filter status=exited --filter status=dead -q
echo docker stop $(docker ps -q)
docker stop $(docker ps -q)
echo docker container prune -f
docker container prune -f
echo docker rm $(docker ps -a -q)
docker rm $(docker ps -a -q)
echo docker image prune -a -f
docker image prune -a -f
echo docker volume prune -a -f
docker volume prune -a -f
echo docker buildx prune -f
docker buildx prune -f
echo docker network prune -f
docker network prune -f
echo docker system prune --volumes -af
docker system prune --volumes -af
echo fully cleaned/pruned all docker artifacts ...
echo docker images
docker images
echo docker ps --filter status=exited --filter status=dead -q
docker ps --filter status=exited --filter status=dead -q
echo docker ps -a
docker ps -a
#echo docker ps -l | tail -1
#docker ps -l | tail -1
echo docker ps -l
docker ps -l


