# sciviz2024

See also: https://gitlab.com/OpenSciViz/cloud2024.git

Above contains latest efforts to setup Openstack and Cloudstack

## Free mesh data

https://casual-effects.com/data

https://github.com/mikedh/trimesh.git

### https://stackoverflow.com/questions/52653734/how-to-do-a-3d-plot-of-gaussian-using-numpy
    from matplotlib import cm

    x=np.linspace(-10,10, num=100)
    y=np.linspace(-10,10, num=100)
    x, y = np.meshgrid(x, y)
    z = np.exp(-0.1*x**2-0.1*y**2)
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    ax.plot_surface(x,y,z, cmap=cm.jet)
    plt.show()

## C, C++, and Python Render

https://github.com/RenderKit

https://github.com/cvg/raybender.git

https://github.com/SAOImageDS9/SAOImageDS9.git -- ANSI C

## Yale Bright Star Catalogue

From https://www.kaggle.com/datasets/alexanderbelopolsky/yale-bright-star-catalog-version-5

The Bright Star Catalogue, also known as the Yale Catalogue of Bright Stars, Yale Bright Star Catalogue, or just YBS, is a star catalogue that lists all stars of stellar magnitude 6.5 or brighter, which is roughly every star visible to the naked eye from Earth. The catalog lists 9,110 objects, of which 9,095 are stars, 11 are novae or supernovae, and four are non-stellar objects which are the globular clusters 47 Tucanae (designated HR 95) and NGC 2808 (HR 3671), and the open clusters NGC 2281 (HR 2496) and Messier 67 (HR 3515).

The catalogue is fixed in number of entries, but its data is maintained, and it is appended with a comments section about the objects that has been steadily enhanced. The abbreviation for the catalog as a whole is BS or YBS but all citations of stars it indexes use HR before the catalog number, a homage to the catalog's direct predecessor, published in 1908, named the Harvard Revised Photometry Catalogue.

These files contain version 5 of the Yale Bright Star Catalog of 9,110 stars, including B1950 positions, proper motions, magnitudes, and, usually, spectral types.

http://tdc-www.harvard.edu/catalogs/bsc5.html

http://tdc-www.harvard.edu/catalogs/bsc5.dat.gz

    References
    Hoffleit, D. and Warren, Jr., W.H., 1991, "The Bright Star Catalog, 5th Revised Edition (Preliminary Version)".

## Scientific Computinh Resources

https://docs.nersc.gov/systems

https://github.com/wrf-model

https://www.amazon.com/Modern-Fortran-Building-Efficient-Applications

https://fortran-lang.discourse.group

## WholesaleInternet Hosts Feb 28 2024

According to recent Openstack installation notes, hosts should have at least 2 drives with 300GB or more each of free space...

https://ubuntu.com/openstack/install

https://docs.openstack.org/install-guide/

New host for openstack and maybe cloudstack: helicity.me and hiyield.us ...

New host should replace 90ghz.org (which lacked large 2nd disk vol) ensuring Openstack compat?

s220144 (aka helicity.me and hiyield.us): inet 69.30.255.210/29 brd 69.30.255.215 scope global ens9

df -h|grep 'dev/sd'

    /dev/sdb2       440G   19G  398G   5% /
    /dev/sda        440G   28K  417G   1% /sda

Note old 90ghz.org was retired after backup to new host and 1percent.us and wehike.us

discarded: hon@90ghz: inet 198.204.224.250/29 brd 198.204.224.255 scope global ens9

    /dev/sda5       961G  436G  477G  48% /
    /dev/sda1       960M  375M  537M  42% /boot

hon@wehike: inet 69.30.253.2/29 brd 69.30.253.7 scope global enp6s0

    /dev/sda5       116G   44G   67G  40% /
    /dev/sda1       960M  208M  703M  23% /boot
    /dev/sdb        2.7T  2.0T  656G  75% /sdb

hon@1percent: inet 69.30.230.122/29 brd 69.30.230.127 scope global ens9

    /dev/sdb2       880G   26G  809G   4% /
    tmpfs            63G     0   63G   0% /dev/shm
    /dev/sda        880G   28K  835G   1% /sda


## Try latest Cloudstack as well as Openstack ...

First attempt resulted in cloudbr0 and cloudbr1 setup that somehow disabled the host network

https://docs.cloudstack.apache.org/en/latest/installguide/index.html

https://docs.cloudstack.apache.org/en/latest/quickinstallationguide/qig.html

## Brython vs RenPy vs PyGame vs Pygbag ???

https://stackabuse.com/an-introductory-guide-to-brython/  and  https://brython.info/static_tutorial/en/index.html


## Python ModernGL and perhaps Pygame/Pygbag/RenPy for astrophysics and earth-and-planetary visualizations

3-D visualizations of Earth biosphere and skies and celestial objects visible to the naked eye.

Contextualize and link content via some sorta audio/visual intertactive novel / game?

https://github.com/pygame-web/pygbag.git

https://externlabs.com/blogs/top-10-python-frameworks-for-game-development

https://www.renpy.org/doc/html/web.html -- HTML5. Investigate mp4 to webm file conversion

https://www.gumlet.com/learn/webm-vs-mp4 -- Evidenty webm is outa vogue while mp4 seems better supported?

https://gitlab.com/OpenSciViz/astro.git -- a bit too much content to submodule (many minutes to clone!)

https://github.com/moderngl/moderngl.git

https://github.com/moderngl/moderngl-window/tree/master/examples -- 10 short example

https://snyk.io/advisor/python/moderngl/example -- 10 short examples

https://pygame-zero.readthedocs.io/en/stable/introduction.html -- eval simpler pygame module(s) vs. renpy

https://github.com/kadir014/pygame-video.git

https://devpost.com/software/pygame-on-web -- WASM

https://github.com/renpy/renpy.git -- attempt to write and deploy an interactive visial novel via RenPy

https://github.com/methanoliver/awesome-renpy.git

=======

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/OpenSciViz/sciviz.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/OpenSciViz/sciviz/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thanks to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Suggestions for a good README

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

## Name
Choose a self-explaining name for your project.

## Description
Let people know what your project can do specifically. Provide context and add a link to any reference visitors might be unfamiliar with. A list of Features or a Background subsection can also be added here. If there are alternatives to your project, this is a good place to list differentiating factors.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

## Support
Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc.

## Roadmap
If you have ideas for releases in the future, it is a good idea to list them in the README.

## Contributing
State if you are open to contributions and what your requirements are for accepting them.

For people who want to make changes to your project, it's helpful to have some documentation on how to get started. Perhaps there is a script that they should run or some environment variables that they need to set. Make these steps explicit. These instructions could also be useful to your future self.

You can also document commands to lint the code or run tests. These steps help to ensure high code quality and reduce the likelihood that the changes inadvertently break something. Having instructions for running tests is especially helpful if it requires external setup, such as starting a Selenium server for testing in a browser.

## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
